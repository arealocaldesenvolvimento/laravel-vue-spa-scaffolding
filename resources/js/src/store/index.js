import Vue from "vue";
import Vuex from "vuex";

import AuthModule from "../views/Auth/store";

Vue.use(Vuex);

const modules = {
    AuthModule
};

export default new Vuex.Store({
    modules
});
